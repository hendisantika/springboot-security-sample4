package com.hendisantika.springbootsecuritysample4.repository;

import com.hendisantika.springbootsecuritysample4.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-sample4
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/09/18
 * Time: 07.26
 * To change this template use File | Settings | File Templates.
 */
public interface UserRepository extends JpaRepository<User, String> {

    User findByUsername(String username);
}