package com.hendisantika.springbootsecuritysample4.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-sample4
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/09/18
 * Time: 07.25
 * To change this template use File | Settings | File Templates.
 */
@Data
@Entity
public class User {

    @Email
    @Size(min = 5, max = 255)
    private String email;

    @Size(min = 5, max = 32)
    @Id
    @Column(length = 32)
    private String username;

    @Size(min = 5, max = 255)
    @Column(nullable = false)
    private String password;

    private boolean enabled;

    @Column(length = 10)
    @Enumerated(EnumType.STRING)
    private Role role;
}