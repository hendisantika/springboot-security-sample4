package com.hendisantika.springbootsecuritysample4;

import com.hendisantika.springbootsecuritysample4.entity.Role;
import com.hendisantika.springbootsecuritysample4.entity.User;
import com.hendisantika.springbootsecuritysample4.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;

@Slf4j
@SpringBootApplication
public class SpringbootSecuritySample4Application {
    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSecuritySample4Application.class, args);
    }

    @PostConstruct
    public void init() {
        User user = new User();
        user.setUsername("admin");
        user.setEmail("admin@millky.com");
        user.setPassword(passwordEncoder.encode("123456"));
        user.setEnabled(true);
        user.setRole(Role.ADMIN);
        userRepository.save(user);
    }
}
